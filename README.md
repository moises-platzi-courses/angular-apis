# Consumo de APIs REST con Angular

Aprende sobre las principales solicitudes HTTP basic en Angular. Descubre las buenas prácticas para el manejo de ambientes y errores. Implementa la autenticación de users a tu tienda en línea.

+ Implementar credenciales en tu tienda en línea
+ Resolver el problema de CORS
+ Aprender sobre el uso de interceptores
+ Manejar archivos con Http

## Contenido del Curso

### Http basic
+ Solicitudes GET
+ Detalle de producto
+ Implementando slides
+ Solicitudes POST
+ Solicitudes PUT y PATCH
+ Solicitudes DELETE
+ Url Parameters / Paginación
+ Observable vs. Promise
+ Reintentar una petición

### Buenas prácticas
+ El problema de CORS
+ Manejo de ambientes
+ Manejo de errores
+ Transformar peticiones
+ Evitando el callback hell

### Auth
+ Login y manejo de Auth
+ Manejo de headers
+ Uso de interceptores
+ Enviar Token con un interceptores
+ Creando un contexto a interceptor

### Archivos
+ Descarga de archivos con Http
+ Subida de archivos con Http
